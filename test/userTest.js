const chai = require("chai");
const request = require("request");

const expect = chai.expect;

describe("Testando rotas do usuário", () => {
  it("Deve retornar todos os websites dos usuários", done => {
    request("http://localhost:8080/websites", (error, response, body) => {
      expect(response.statusCode).to.equal(200);
      expect(body.length).to.equal(136);

      done();
    });
  });

  it("Deve retornar os usuários em ordem alfabética", done => {
    request(
      "http://localhost:8080/alphabeticalOrder",
      (error, response, body) => {
        const parsedBody = JSON.parse(body);

        expect(response.statusCode).to.equal(200);
        expect(parsedBody[0].nome).to.equal("Chelsey Dietrich");
        expect(parsedBody[parsedBody.length - 1].nome).to.equal(
          "Patricia Lebsack"
        );

        done();
      }
    );
  });

  it("Deve retornar somente os usuários cujo o endereço tenha a palavra 'Suite'", done => {
    request("http://localhost:8080/suites", (error, response, body) => {
      const parsedBody = JSON.parse(body);

      expect(response.statusCode).to.equal(200);

      for (let i = 0; parsedBody.length > i; i++) {
        expect(parsedBody[i].endereco.indexOf("Suite")).to.not.equal(-1);
      }

      done();
    });
  });
});
