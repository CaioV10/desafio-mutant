const router = require("./routes/userRoute");
const express = require("express");
const app = express();

const port = 8080;
const userRoute = "http://localhost:8080/";

app.use(router.exec);

const server = app.listen(8080, () => {
  console.log(`Servidor funcionando na porta ${port}`);
});

app.get("/", (request, response) => {
  response.send(
    `As rotas disponíveis são: <br> 
    <a href=${userRoute}websites>${userRoute}websites</a> <br>
    <a href=${userRoute}alphabeticalOrder>${userRoute}alphabeticalOrder</a> <br>
    <a href=${userRoute}suites>${userRoute}suites</a>`
  );
});
