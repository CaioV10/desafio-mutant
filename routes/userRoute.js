const router = require("express").Router();
const userController = require("../controllers/userController");

router.get("/websites", userController.getWebsites);
router.get("/alphabeticalOrder", userController.getAlphabeticalOrder);
router.get("/suites", userController.getSuite);

exports.exec = router;
