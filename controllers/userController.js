const request = require("request");
// const logConfig = require("../utils/logConfig");

// const clientElastic = logConfig.runElastic;

const makeRequest = url =>
  new Promise((resolve, reject) => {
    request(url, (error, response, body) => {
      if (error) {
        reject(error);
      } else {
        resolve(body);
      }
    });
  });

exports.getAlphabeticalOrder = async (request, response) => {
  const body = await makeRequest("https://jsonplaceholder.typicode.com/users");
  let dataStructure = {};
  const resData = [];

  const parsedData = JSON.parse(body);

  for (let i = 0; parsedData.length > i; i++) {
    dataStructure = {
      nome: parsedData[i].name,
      email: parsedData[i].email,
      empresa: parsedData[i].company.name
    };

    resData.push(dataStructure);
  }

  resData.sort((a, b) => a.nome.localeCompare(b.nome));

  response.send(resData);

  // clientElastic.index({
  //   index: "request",
  //   type
  // });
};

exports.getWebsites = async (request, response) => {
  const body = await makeRequest("https://jsonplaceholder.typicode.com/users");
  const resData = [];

  const parsedData = JSON.parse(body);

  for (let i = 0; parsedData.length > i; i++) {
    resData.push(parsedData[i].website);
  }

  response.send(resData);
};

exports.getSuite = async (request, response) => {
  const body = await makeRequest("https://jsonplaceholder.typicode.com/users");
  let dataStructure = {};
  const resData = [];

  const parsedData = JSON.parse(body);

  for (let i = 0; parsedData.length > i; i++) {
    if (parsedData[i].address.suite.indexOf("Suite") != -1) {
      dataStructure = {
        usuario: parsedData[i].username,
        endereco: parsedData[i].address.suite
      };

      resData.push(dataStructure);
    }
  }

  response.send(resData);
};
