const elasticSearch = require("elasticsearch");

exports.runElastic = () => {
  const client = new elasticSearch.Client({
    hosts: ["http://testeElastic:123@test:3030"]
  });

  client.ping({ requestTimeout: 10000 }, err => {
    if (err) {
      console.log("Elasticsearch caiu");
    } else {
      console.log("Elasticsearch está rodando");
    }
  });

  client.indices.create(
    {
      index: "request"
    },
    function(err, resp, status) {
      if (err) {
        console.log(err);
      } else {
        console.log("create", resp);
      }
    }
  );

  return client;
};
